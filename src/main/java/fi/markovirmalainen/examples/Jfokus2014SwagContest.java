package fi.markovirmalainen.examples;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Jfokus2014SwagContest {
    public List<Integer> removeOddNumbersAndNumberEight(final List<Integer> numbers) {
        return numbers.stream().filter(n -> n.intValue() != 8 && n.intValue() % 2 == 0).collect(Collectors.toList());
    }

    public static interface Encryption {
        String applyEncryptionTo(String input);
    }

    public Encryption getReverseEncryption() {
        return (String input) -> new StringBuilder(input).reverse().toString();
    }
}
